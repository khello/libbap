(* bap_cfg.ml                                                              *)
(* Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>                   *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Volatility

let int_to_python = Bap_ast.int_to_python

let int64_to_python = Bap_ast.int64_to_python

let bbid_to_python = function
  | Cfg.BB_Entry ->
  		PyString "bb_entry"
  | Cfg.BB_Exit ->
  		PyString "bb_exit"
  | Cfg.BB_Indirect ->
  		PyString "bb_indirect"
  | Cfg.BB_Error ->
  		PyString "bb_error"
  | Cfg.BB(n) ->
  		PyDict [ (PyString "bb", int_to_python n) ]

let node_to_python cfg node py_nodes =
	let py_stmts = Bap_ast.to_python(Cfg.AST.get_stmts cfg node) in
	let py_label = bbid_to_python(Cfg.AST.G.V.label node) in
	let py_hash = int_to_python(Cfg.AST.G.V.hash node) in
	let py_node = PyDict [ (PyString "hash", py_hash); (PyString "label", py_label); (PyString "stmts", py_stmts) ] in
		py_node :: py_nodes

let edge_to_python edge py_edges =
  let label = Cfg.AST.G.E.label edge in
  let py_source = int_to_python(Cfg.AST.G.V.hash(Cfg.AST.G.E.src edge)) in
  let py_target = int_to_python(Cfg.AST.G.V.hash(Cfg.AST.G.E.dst edge)) in
  let py_edge =
    match label with
    | Some b ->
        PyDict [ (PyString "source", py_source); (PyString "target", py_target); (PyString "label", PyBool b) ]
    | None ->
        PyDict [ (PyString "source", py_source); (PyString "target", py_target) ]
  in
    py_edge :: py_edges

let to_python cfg =
  let py_nodes = Cfg.AST.G.fold_vertex (node_to_python cfg) cfg [] in
  let py_edges = Cfg.AST.G.fold_edges_e edge_to_python cfg [] in
    PyDict [ (PyString "nodes", PyList py_nodes); (PyString "edges", PyList py_edges) ]
