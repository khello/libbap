#!/usr/bin/env python
#
# analysis.py
# Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details. 
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 

#---------------------------------------------------------------
# Analysis Commands
#---------------------------------------------------------------

def typecheck():
  "Typecheck program"
  return [["iltrans", "-typecheck"], ["control", "-return"]]

def vsa():
  """
  Value set analysis (VSA) - loops and specials are not currently handled.
  """
  return [["iltrans", "-vsa"]]

def struct():
  """
  Structural analysis.

  Algorithm based on Advanced Compiler Design & Implementation by Steven S Muchnick.
  """
  return [["iltrans", "-struct"]]

def undef_vars():
  "Decorate control flow graph (CFG) with variable undefined attributes (expressed as a CTL* formula)"
  return [["iltrans", "-undef-vars"]]

def sccvn():
  "Apply Strongly Connected Component based Value Numbering"
  return [["iltrans", "-sccvn"]]

def deadcode(aggressive=False):
  """
  Perform dead code elimination

  <aggressive> if True, perform aggressive dead code ellimination
  """
  if aggressive:
  	return [["iltrans", "-adeadcode"]]
  else:
  	return [["iltrans", "-deadcode"]]

def jumpelim():
  "Control flow optimization."
  return [["iltrans", "-jumpelim"]]

def uniqueify_labels():
  "Ensure all labels are unique"
  return [["iltrans", "-uniqueify-labels"]]

def replace_unknowns():
  "Replace all unknowns with zeros"
  return [["iltrans", "-replace-unknowns"]]

def bberror_assume_false():
  "Add an \"assume false\" statement to BB_Error and add an edge to BB_Exit"
  return [["iltrans", "-bberror-assume-false"]]

def normalize_mem():
  "Normalize memory accesses as array accesses"
  return [["iltrans", "-normalize-mem"]]

def flatten_mem():
  "Flatten memory accesses"
  return [["iltrans", "-flatten-mem"]]

def usedef():
  "Compute and print use def chains"
  return [["iltrans", "-usedef"], ["control", "-return"]]

def defuse():
  "Compute and print def use chains"
  return [["iltrans", "-defuse"], ["control", "-return"]]
